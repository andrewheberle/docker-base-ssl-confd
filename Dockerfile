FROM registry.gitlab.com/andrewheberle/docker-base-ssl:1.5

MAINTAINER Andrew Heberle (https://gitlab.com/andrewheberle/docker-base-ssl-confd)

# Confd environment
ENV CONFD_BACKEND=env \
    CONFD_MAX_FAILCOUNT=5 \
    CONFD_OPTS= \
    CONFD_UPDATE_INTERVAL=30 \
    CONFD_VCS_URL=github.com/kelseyhightower/confd \
    CONFD_VERSION=0.14.0

# Download and compile confd
RUN apk add --update --no-cache --virtual .confd-build-deps go git gcc musl-dev && \
    mkdir -p /src/go/src/$CONFD_VCS_URL && \
    git clone --quiet --branch "v$CONFD_VERSION" https://$CONFD_VCS_URL.git /src/go/src/$CONFD_VCS_URL && \
    cd /src/go/src/$CONFD_VCS_URL && \
    GOPATH=/src/go GOOS=linux CGO_ENABLED=0 go build -a -ldflags '-extldflags "-static"' . && \
    mv ./confd /bin/ && \
    chmod +x /bin/confd && \
    apk del .confd-build-deps && \
    rm -rf /src

# Copy root fs skelton
COPY root /

ARG BUILD_DATE
ARG VCS_REF
ARG VERSION
ARG NAME="docker-base-ssl-confd"
ARG DESCRIPTION="Base SSL image with confd"

LABEL name="$NAME" \
      version="$VERSION" \
      architecture="amd64" \
      description="$DESCRIPTION" \
      org.label-schema.description="$DESCRIPTION" \
      org.label-schema.build-date="$BUILD_DATE" \
      org.label-schema.name="$NAME" \
      org.label-schema.vcs-ref="$VCS_REF" \
      org.label-schema.vcs-url="https://gitlab.com/andrewheberle/$NAME" \
      org.label-schema.version="$VERSION" \
      org.label-schema.schema-version="1.0"
